# Use the official PHP image with Apache.
FROM php:8.1

# Install PHP Extensions
RUN apt-get update && \
    apt-get install -y libcurl4-openssl-dev libsqlite3-dev && \
    docker-php-ext-install curl pdo_sqlite

# Set the document root to /var/www/html
WORKDIR /var/www/html

# Expose port 8000
EXPOSE 8000

# By default, start PHP in the foreground
CMD ["php", "-S", "0.0.0.0:8000", "src/index.php"]