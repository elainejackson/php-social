<?php
function beanToMastoApiAccount($bean) {
    return array(
        'id' => strval($bean->id),
        'acct' => $bean->username,
        'display_name'  => $bean->username,
        'note' => $bean->note,
        'followers_count' => 0,
        'following_count' => 0,
        'statuses_count' => $bean->statuses_count,
        'avatar' => $bean->avatar,
        'avatar_static' => $bean->avatar_static,
        'header' => $bean->header,
        'header_static' => $bean->header_static,
        'bot' => (bool) $bean->is_bot,
        'is_locked' => (bool) $bean->is_locked,
        'uri' => $GLOBALS["SCHEME"] . $_SERVER['HTTP_HOST'].'/users/'.$bean->username,
        'url' => $GLOBALS["SCHEME"] . $_SERVER['HTTP_HOST'].'/users/'.$bean->username,
        'fqn' => $bean->username . '@' . $_SERVER['HTTP_HOST'],
        'role' => null,
    );
}