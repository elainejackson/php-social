<?php

function beanToMastoApiStatus($bean, $currentAccountID): array
{
    $response = array();
    $response['application'] = $bean->application ? $bean->application->export() : null;
    $response['bookmarked'] = isStatusBookmarked($currentAccountID, $bean->id);
    $response['content'] = $bean->status;
    $response['created_at'] = $bean->created_at;
    $response['edited_at'] = $bean->edited_at;
    $response['emojis'] = array_map(function($emoji) {
        return $emoji->export();
    }, $bean->sharedEmojiList); // Assuming there's a shared list for emojis
    $response['favourited'] = isStatusFavorited($currentAccountID, $bean->id);
    $response['favourites_count'] = countFavorites($bean->id);
    $response['id'] = $bean->id;
    $response['in_reply_to_account_id'] = $bean->in_reply_to_account_id;
    $response['in_reply_to_id'] = $bean->in_reply_to_id;
    $response['language'] = $bean->language;
    $response['media_attachments'] = array_map(function($attachment) {
        return $attachment->export();
    }, $bean->sharedAttachmentList);
    $response['mentions'] = array_map(function($mention) {
        return $mention->export();
    }, $bean->sharedMentionList);
    $response['muted'] = (boolean) $bean->muted;
    $response['pinned'] = (boolean) $bean->pinned;
    $response['reblog'] = $bean->reblog ? $bean->reblog->export() : null;
    $response['reblogged'] = (boolean) $bean->reblogged;
    $response['reblogs_count'] = (int) $bean->reblogs_count;
    $response['replies_count'] = (int) $bean->replies_count;
    $response['sensitive'] = (boolean) $bean->sensitive;
    $response['spoiler_text'] = $bean->spoiler_text;
    $response['tags'] = array_map(function($tag) {
        return $tag->export();
    }, $bean->sharedTagList);
    $response['uri'] = $bean->uri;
    $response['url'] = $bean->url;
    $response['visibility'] = $bean->visibility;

    $account = R::load('accounts', $bean->account_id);
    $response['account'] = [
        'id' => $account['id'],
        'url' => $account['url'],
        'uri' => $account['url'],
        'acct' => $account['username'],
        'display_name' => $account['display_name'],
        'note' => $account['note'],
        'followers_count' => 0,
        'following_count' => 0,
        'statuses_count' => 0,
        'avatar' => '',
        'avatar_static' => '',
        'header' => '',
        'header_static' => '',
        'bot' => (bool) $account['is_bot'],
        'is_locked' => (bool) $account['is_locked'],
    ];

    // Adding poll info if the status has a poll associated
    if ($bean->poll) {
        $poll = $bean->poll->export();
        $options = json_decode($poll['options']);
        $pollResult = [];

        for ($i = 0; $i < count($options); $i += 2) {
            // Check if there's a next element for votes_count
            if (isset($options[$i+1])) {
                $pollResult[] = [
                    'title' => $options[$i],
                    'votes_count' => 0
                ];
            }
        }

        $poll['options'] = $pollResult;

        $poll['multiple'] = (bool) $poll['multiple'];
        $poll['expires_at'] = null;
        $poll['expired'] = false;
        $poll['votes_count'] = 0;
        $poll['voter_count'] = 0;
        $poll['emojis'] = [];
        $response['poll'] = $poll;
    } else {
        $response['poll'] = null;
    }

    return $response;
}