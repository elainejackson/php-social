<?php

require_once "Request.php";

class Router {
    private $routes = [];
    private $middlewares = [];

    public function get($path, $callback): void
    {
        $this->routes['GET'][$path] = $callback;
    }

    public function post($path, $callback): void
    {
        $this->routes['POST'][$path] = $callback;
    }

    public function patch($path, $callback): void
    {
        $this->routes['PATCH'][$path] = $callback;
    }

    public function put($path, $callback): void
    {
        $this->routes['PUT'][$path] = $callback;
    }

    public function delete($path, $callback): void
    {
        $this->routes['DELETE'][$path] = $callback;
    }

    public function middleware($middleware): void
    {
        $this->middlewares[] = $middleware;
    }

    /**
     * Dispatches the current request to the appropriate route callback.
     *
     * Usage:
     *
     * 1. Define routes with path parameters using the colon (:) notation.
     *    For example: $router->delete('/api/v1/lists/:id', 'handleDelete');
     *
     * 2. Define the route's callback function to accept the path parameters as arguments.
     *    For the above example:
     *    function handleDelete($id) {
     *        echo "Deleting item with ID: " . $id;
     *    }
     *
     *    When the user accesses `/api/v1/lists/123`, `handleDelete` will be called
     *    and `$id` will be set to `123`.
     *
     * 3. For routes with multiple parameters, the parameters will be passed to the
     *    callback function in the order they appear.
     *    For example, for the route:
     *    $router->get('/api/v1/users/:userId/posts/:postId', 'handlePost');
     *
     *    The `handlePost` function should be defined as:
     *    function handlePost($userId, $postId) {
     *        echo "Displaying post with ID: " . $postId . " for user with ID: " . $userId;
     *    }
     *
     *    Accessing `/api/v1/users/45/posts/789` will call `handlePost` with `$userId`
     *    set to `45` and `$postId` set to `789`.
     *
     */
    public function dispatch(): void
    {
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $requestMethod = $_SERVER["REQUEST_METHOD"];

        // Execute middlewares
        $request = new Request();
        foreach ($this->middlewares as $middleware) {
            $middleware($request);
        }

        $routeFound = false;

        if (isset($this->routes[$requestMethod])) {
            foreach ($this->routes[$requestMethod] as $routePattern => $callback) {
                $pattern = "@^" . preg_replace("/:(\w+)/", "([a-zA-Z0-9_-]+)", $routePattern) . "$@D";

                if (preg_match($pattern, $uri, $matches)) {
                    array_shift($matches);
                    $request->params = $matches;
                    call_user_func_array($callback, [$request]);
                    $routeFound = true;
                    break;
                }
            }
        }

        if (!$routeFound) {

            if ($uri == '/') {
                $uri = 'index.html';
            }

            $relativePath = 'static' . DIRECTORY_SEPARATOR . ltrim(str_replace('/', DIRECTORY_SEPARATOR, $uri), DIRECTORY_SEPARATOR);
            $filePath = __DIR__ . DIRECTORY_SEPARATOR . $relativePath;

            // Check if the file exists
            if (file_exists($filePath)) {
                // Get the content of the file
                $response = file_get_contents($filePath);

                // Get the content type based on the file extension
                $contentType = getContentTypeByExtension($filePath);

                // Set the content type header
                if ($contentType) {
                    header("Content-Type: $contentType");
                }

                // Output the content
                echo $response;
            } else {
                // If file doesn't exist, you can handle the error or redirect as necessary.
                header("HTTP/1.0 404 Not Found");
                $relativePath = 'static' . DIRECTORY_SEPARATOR . ltrim(str_replace('/', DIRECTORY_SEPARATOR, 'index.html'), DIRECTORY_SEPARATOR);
                $filePath = __DIR__ . DIRECTORY_SEPARATOR . $relativePath;
                if (file_exists($filePath)) {
                    // Get the content of the file
                    $response = file_get_contents($filePath);

                    // Get the content type based on the file extension
                    $contentType = getContentTypeByExtension($filePath);

                    // Set the content type header
                    if ($contentType) {
                        header("Content-Type: $contentType");
                    }

                    // Output the content
                    echo $response;
                }
            }
        }
    }
}

function getContentTypeByExtension($filePath): string
{
    $extension = pathinfo($filePath, PATHINFO_EXTENSION);
    $mimeTypes = [
        'html' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'jpg' => 'image/jpeg',
        'png' => 'image/png',
        'svg' => 'image/svg+xml',
    ];
    return $mimeTypes[strtolower($extension)] ?? 'text/plain';
}