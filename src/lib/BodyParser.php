<?php

class BodyParser {
    public function __invoke($request) {
        $contentType = $_SERVER["CONTENT_TYPE"] ?? '';

        $content = file_get_contents("php://input");

        if (str_contains($contentType, "application/x-www-form-urlencoded")) {
            parse_str($content, $request->body);
        } elseif (str_contains($contentType, "application/json")) {
            $request->body = json_decode($content, true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                error_log('JSON decode error: ' . json_last_error_msg());
            } else {
                error_log('Parsed JSON body: ' . print_r($request->body, true));
            }
        } elseif (str_contains($contentType, "multipart/form-data")) {
            $request->body = $this->decodeMultipartFormData($content, $contentType);              // TODO: Add logic for handling file uploads
        }
    }

    private function getBoundary($contentType) {
        preg_match('/boundary=(.*)$/', $contentType, $matches);
        return $matches[1] ?? null;
    }

    private function decodeMultipartFormData($content, $contentType) {    $boundary = $this->getBoundary($contentType);
        $parts = array_slice(explode($boundary, $content), 1, -1);  // Exclude the first and last parts which are empty
        $data = [];

        foreach ($parts as $part) {
            // If this part is empty, skip it
            if (trim($part) == "") continue;

            // Find position of actual content
            $nameStart = strpos($part, 'name="') + 6;
            $nameEnd = strpos($part, '"', $nameStart);
            $name = substr($part, $nameStart, $nameEnd - $nameStart);

            // Extracting the value
            $valueStart = strpos($part, "\r\n\r\n") + 4; // Skip 2 CRLFs to get to the value
            $valueEnd = strpos($part, "\r\n--", $valueStart);  // Locate end of value before the boundary starts
            $value = ($valueEnd === false) ? trim(substr($part, $valueStart)) : trim(substr($part, $valueStart, $valueEnd - $valueStart));

            $data[$name] = $value;
        }

        return $data;
    }
}


