<?php

class Request {
    public $body;
    public $params;
    public $query;
    public $db;
    public $user;

    public function __construct() {
        $this->body = [];
        $this->params = [];
        $this->query = $_GET;
        $this->db = null;
        $this->user = null;
    }
}
