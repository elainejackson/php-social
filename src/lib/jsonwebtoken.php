<?php

/**
 * JsonWebToken Class
 *
 * This class is designed to provide simple methods for creating and validating JSON Web Tokens (JWTs).
 * The class currently supports the HS256 algorithm.
 *
 * Usage Examples:
 *
 * Creating a JWT:
 * ```php
 * $jwt = new JsonWebToken('your_secret_key');
 * $token = $jwt->create(['userId' => 1234, 'role' => 'admin']);
 * echo $token; // Outputs the JWT.
 * ```
 *
 * Validating a JWT:
 * ```php
 * $jwt = new JsonWebToken('your_secret_key');
 * try {
 *     $payload = $jwt->validate($token);
 *     print_r($payload); // Outputs the token payload if the token is valid.
 * } catch (Exception $e) {
 *     echo "Error: " . $e->getMessage(); // Outputs error message if the token is not valid.
 * }
 * ```
 */
class JsonWebToken {
    private $secret;

    /**
     * Constructor to initialize the JsonWebToken class.
     *
     * @param string $secret The secret key used for signing the JWT.
     */
    public function __construct($secret) {
        $this->secret = $secret;
    }

    /**
     * Encodes data to Base64Url format.
     *
     * @param mixed $data The data to be encoded.
     * @return string The Base64Url encoded data.
     */
    private function base64UrlEncode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    /**
     * Decodes Base64Url encoded data.
     *
     * @param string $base64Url The Base64Url encoded data.
     * @return mixed The decoded data.
     */
    private function base64UrlDecode($base64Url) {
        return base64_decode(strtr($base64Url, '-_', '+/') . str_repeat('=', 3 - (3 + strlen($base64Url)) % 4));
    }

    /**
     * Creates a new JWT with the provided payload.
     *
     * @param array $payload The payload for the JWT.
     * @return string The generated JWT.
     */
    public function create(array $payload) {
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
        $headerBase64 = $this->base64UrlEncode($header);

        $payloadBase64 = $this->base64UrlEncode(json_encode($payload));

        $signature = hash_hmac('sha256', "$headerBase64.$payloadBase64", $this->secret, true);
        $signatureBase64 = $this->base64UrlEncode($signature);

        return "$headerBase64.$payloadBase64.$signatureBase64";
    }

    /**
     * Validates the provided JWT and returns its payload.
     *
     * @param string $jwt The JWT to be validated.
     * @return array The payload of the validated JWT.
     * @throws Exception If the JWT is not valid or uses an unexpected algorithm.
     */
    public function validate($jwt) {
        [$headerBase64, $payloadBase64, $signatureBase64] = explode('.', $jwt);

        $header = json_decode($this->base64UrlDecode($headerBase64), true);
        if ($header['alg'] !== 'HS256') {
            throw new Exception('Unexpected algorithm');
        }

        $signature = $this->base64UrlDecode($signatureBase64);
        $validSignature = hash_hmac('sha256', "$headerBase64.$payloadBase64", $this->secret, true);

        if (hash_equals($signature, $validSignature)) {
            return json_decode($this->base64UrlDecode($payloadBase64), true);
        } else {
            throw new Exception('Invalid signature');
        }
    }
}