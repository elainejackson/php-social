<?php

class StatusController
{
    public function createStatus($request) {
        // Extract request body
        $data = $request->body;

        // Initialize errors array
        $errors = [];

        // Validate content_type (only checks if it's a string, further validation like allowed MIME types can be added)
        if (isset($data['content_type']) && !is_string($data['content_type'])) {
            $errors[] = "Invalid content_type.";
        }

        // Validate expires_in (must be longer than an hour)
        if (isset($data['expires_in'])) {
            if (!is_int($data['expires_in']) || $data['expires_in'] <= 3600) {
                $errors[] = "expires_in must be an integer and longer than an hour.";
            }
        }

        // Validate in_reply_to_conversation_id and in_reply_to_id
        $validIdPattern = "/^[a-zA-Z0-9]+$/"; // Simple alphanumeric pattern for IDs, adjust as needed.
        foreach (['in_reply_to_conversation_id', 'in_reply_to_id'] as $field) {
            if (isset($data[$field]) && (!is_string($data[$field]) || !preg_match($validIdPattern, $data[$field]))) {
                $errors[] = "Invalid $field.";
            }
        }

        // Validate language (just checks for string type here, further validation can be added)
        if (isset($data['language']) && !is_string($data['language'])) {
            $errors[] = "Invalid language code.";
        }

        // Validate media_ids (checks if it's an array of strings)
        if (isset($data['media_ids']) && (!is_array($data['media_ids']) || !all_string($data['media_ids']))) {
            $errors[] = "media_ids must be an array of strings.";
        }

        // Validate preview (can be boolean, string, or integer)
        if (isset($data['preview']) && !is_bool($data['preview']) && !is_string($data['preview']) && !is_int($data['preview'])) {
            $errors[] = "Invalid preview value.";
        }

        // Validate scheduled_at
        if (isset($data['scheduled_at']) && !validate_datetime($data['scheduled_at'])) {
            $errors[] = "Invalid scheduled_at value.";
        }

        // Validate sensitive (can be boolean, string, or integer)
        if (isset($data['sensitive']) && !is_bool($data['sensitive']) && !is_string($data['sensitive']) && !is_int($data['sensitive'])) {
            $errors[] = "Invalid sensitive value.";
        }

        // Validate spoiler_text and status (just checks for string type here)
        foreach (['spoiler_text', 'status'] as $field) {
            if (isset($data[$field]) && !is_string($data[$field])) {
                $errors[] = "Invalid $field.";
            }
        }

        // Validate to (checks if it's an array of strings)
        if (isset($data['to']) && (!is_array($data['to']) || !all_string($data['to']))) {
            $errors[] = "to must be an array of strings.";
        }

        // Validate visibility
        $allowedVisibilities = ["direct", "private", "unlisted", "public"];
        if (isset($data['visibility']) && (!is_string($data['visibility']) || (!in_array($data['visibility'], $allowedVisibilities) && !preg_match("/^list:\w+$/", $data['visibility'])))) {
            $errors[] = "Invalid visibility value.";
        }

        // If errors exist, send a response back
        if (count($errors) > 0) {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode([
                "errors" => $errors
            ]);
            exit();
        }

        // Begin the transaction
        R::begin();

        try {
            // Create a new status bean
            $status = R::dispense('status');

            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    $status->$key = json_encode($value);
                } else {
                    $status->$key = $value;
                }
            }

            $status->created_at = date("Y-m-d\TH:i:s.000\Z");
            $status->account_id = $request->user->account_id;

            // Handle the poll if provided
            if (isset($data['poll'])) {
                $poll = R::dispense('poll');
                foreach ($data['poll'] as $key => $value) {
                    if (is_array($value)) {
                        $poll->$key = json_encode($value);
                    } else {
                        $poll->$key = $value;
                    }
                }
                R::store($poll);
                $status->poll_id = $poll->id;
            }

            // Store the status
            R::store($status);

            // After-create actions
            $status->url = $GLOBALS['SCHEME'].$_SERVER['HTTP_HOST'].'/notice/'.$status->id;
            $status->uri = $GLOBALS['SCHEME'].$_SERVER['HTTP_HOST'].'/objects/'.$status->id;
            $status->updated_at = date("Y-m-d\TH:i:s.000\Z");
            R::store($status);

            // Increment status count
            $account = R::load('accounts', $status->account_id);
            $account['statuses_count'] =
                (int) $account['statuses_count'] + 1;
            R::store($account);

            // Commit the transaction
            R::commit();

            // Create the response
            $response = beanToMastoApiStatus($status, $request->user->account_id);

            // Send success response to the user
            header("HTTP/1.1 200 OK");
            echo json_encode($response);
        } catch (Exception $e) {
            // If any error occurs during the transaction, roll it back
            R::rollback();

            error_log($e);

            header("HTTP/1.1 500 Internal Server Error");
            echo json_encode([
                "error" => "server_error",
                "error_description" => "An error occurred while posting the status."
            ]);
        }

    }

    public function deleteStatusById($request) {
        $id = $request->params['id'];
    }

    public function getStatusById($request) {
        $status = R::load('status', (int) $request->params[0]);

        if (!$status) {
            header("HTTP/1.1 404 Not Found");
            echo json_encode(["error" => "Status not found."]);
            exit;
        }

        $response = beanToMastoApiStatus($status, $request->user->account_id);

        // Return the result
        header("HTTP/1.1 200 OK");
        header("Content-Type: application/json");
        echo json_encode($response, JSON_UNESCAPED_SLASHES);

    }

    public function getStatusContext($request) {
        // Fetch the target status.
        $status = R::load('statuses', $request->params[0]);

        // If status doesn't exist, return a not-found response.
        if (!$status) {
            header("HTTP/1.1 404 Not Found");
            echo json_encode([
                "error" => "Status not found."
            ]);
            exit;
        }

        $ancestors = getAncestors($status);
        $descendants = getDescendants($status->id);

        // Convert the RedBean objects to arrays or your desired output format.
        $ancestors = array_map(function($status) {
            return $status->export();
        }, $ancestors);

        $descendants = array_map(function($status) {
            return $status->export();
        }, $descendants);

        // Return the result.
        header("HTTP/1.1 200 OK");
        echo json_encode([
            "ancestors" => $ancestors,
            "descendants" => $descendants
        ]);
    }

    public function bookmarkStatus($request): void
    {
        // Retrieve the account ID and status ID from the request
        $accountID = $request->user->account_id;
        $statusID = $request->params[0];

        // Load the account and status
        $account = R::load('accounts', $accountID);
        $status = R::load('status', $statusID);

        // Check if the relationship already exists to avoid duplicate bookmarks
        $existingBookmark = R::findOne('bookmark', 'account_id = ? AND status_id = ?', [$accountID, $statusID]);

        if (!$existingBookmark) {
            // Create a new bookmark
            $bookmark = R::dispense('bookmark');
            $bookmark->account = $account;
            $bookmark->status = $status;

            // Store the bookmark
            R::store($bookmark);

            // Optionally, you can return a success response or any indication that the status has been bookmarked
            http_response_code(200);
            echo json_encode(array(
                'message' => "Status has been bookmarked!",
            ));
        } else {
            // If the relationship already exists, you can return a message indicating that.
            http_response_code(500);
            echo json_encode(array(
                'message' => "Status is already bookmarked by this account.",
            ));
        }
    }

    public function favoriteStatus($request) {
        // Retrieve the account ID and status ID from the request
        $accountID = $request->user->account_id;
        $statusID = $request->params[0];

        // Load the account and status
        $account = R::load('accounts', $accountID);
        $status = R::load('status', $statusID);

        // Check if the relationship already exists to avoid duplicate favorites
        $existingFavorite = R::findOne('favorite', 'account_id = ? AND status_id = ?', [$accountID, $statusID]);

        if (!$existingFavorite) {
            // Create a new favorite
            $favorite = R::dispense('favorite');
            $favorite->account = $account;
            $favorite->status = $status;

            // Store the favorite
            R::store($favorite);

            // Optionally, you can return a success response or any indication that the status has been favorited
            http_response_code(200);
            echo json_encode(array(
                'message' => "Status has been favorited!",
            ));
        } else {
            // If the relationship already exists, you can return a message indicating that.
            http_response_code(500);
            echo json_encode(array(
                'message' => "Status is already favorited by this account.",
            ));
        }
    }
}

// Helper function to check if all elements in array are strings
function all_string(array $arr): bool
{
    return empty(array_filter($arr, function($item) {
        return !is_string($item);
    }));
}

// Helper function to validate datetime
function validate_datetime($datetime): bool
{
    $format = 'Y-m-d\TH:i:sP'; // ISO 8601 format
    $d = DateTime::createFromFormat($format, $datetime);
    return $d && $d->format($format) == $datetime;
}

// Recursive function to retrieve ancestors.
function getAncestors($status): array
{
    $ancestors = [];

    if ($status->in_reply_to_id) {
        $parent = R::load('statuses', $status->in_reply_to_id);
        if ($parent->id) {
            $ancestors[] = $parent;
            $ancestors = array_merge($ancestors, getAncestors($parent));
        }
    }

    return $ancestors;
}

// Recursive function to retrieve descendants.
function getDescendants($statusId): array
{
    $descendants = [];

    $children = R::find('statuses', 'in_reply_to_id = ?', [$statusId]);

    if (!empty($children)) {
        foreach ($children as $child) {
            $descendants[] = $child;
            $descendants = array_merge($descendants, getDescendants($child->id));
        }
    }

    return $descendants;
}

function countFavorites($statusID): int
{
    // Count the number of favorites for a given status
    $favoritesCount = R::count('favorite', 'status_id = ?', [$statusID]);

    return $favoritesCount;
}

function countBookmarks($statusID): int
{
    // Count the number of bookmarks for a given status
    $bookmarksCount = R::count('bookmark', 'status_id = ?', [$statusID]);

    return $bookmarksCount;
}
