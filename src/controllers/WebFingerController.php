<?php

class WebFingerController
{
    public function get($request) {
        $resource = $request->query['resource'] ?? null;
        $type = '';
        $name = '';

        if ($resource && strpos($resource, ':') !== false) {
            list($type, $name) = explode(':', $resource);
        }

        if ($type !== 'acct') {
            header('HTTP/1.1 404 Not Found');
            echo 'Error: Not Found';
            exit;
        }

        $account = true;

        if ($account) {
            $domain = $_SERVER['HTTP_HOST'];
            $splitName = explode('@', $name)[0];
            $baseURL = "https://{$domain}/users/{$splitName}";

            $xml = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
    <Subject>{$type}:{$name}</Subject>
    <Alias>{$baseURL}</Alias>
    <Link href="{$baseURL}" rel="http://webfinger.net/rel/profile-page" type="text/html"/>
    <Link href="{$baseURL}" rel="self" type="application/activity+json"/>
    <Link href="{$baseURL}" rel="self" type="application/ld+json; profile="https://www.w3.org/ns/activitystreams"/>
</XRD>
XML;

            header('Content-Type: application/xml');
            echo $xml;
        } else {
            header('HTTP/1.1 404 Not Found');
            echo 'Error: Not Found';
        }
    }
}