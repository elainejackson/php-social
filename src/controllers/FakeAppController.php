<?php
class FakeAppController {

    private const FAKE_APP = [
        "id" => "1",
        "name" => "Celestia",
        "website" => null,
        "redirect_uri" => "urn:ietf:wg:oauth:2.0:oob",
        "client_id" => "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
        "client_secret" => "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
        "vapid_key" => "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=",
    ];

    public function getApp() {
        header('Content-Type: application/json');
        echo json_encode(self::FAKE_APP);
    }

    public function postApp() {
        header('Content-Type: application/json');

        $data = json_decode(file_get_contents('php://input'), true);

        $redirect_uri = self::FAKE_APP["redirect_uri"];

        if (isset($data["redirect_uris"])) {
            $redirect_uri = $data["redirect_uris"];
        }

        $response = array_merge(self::FAKE_APP, ["redirect_uri" => $redirect_uri]);
        echo json_encode($response);
    }

    public function getPleromaApps() {
        header('Content-Type: application/json');

        $response = array_merge(self::FAKE_APP, ["redirect_uri" => ""]);
        echo json_encode($response);
    }
}