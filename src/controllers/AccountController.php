<?php

class AccountController
{
    public function relationship($request): void {

    }

    public function followUser($request): void
    {
        $followerAccountID = $request->user->account_id; // ID of the follower account
        $followedUsername = $request->params[0]; // Username of the account to be followed

        // Get the follower and followed accounts
        $followerAccount = R::load('accounts', $followerAccountID);
        $followedAccount = R::findOne('accounts', 'username = ?', [$followedUsername]);

        if ($followerAccount && $followedAccount) {
            // Check if the relationship already exists to avoid duplicate follows
            $existingFollow = R::findOne('follow', 'follower_id = ? AND followed_id = ?', [$followerAccountID, $followedAccount->id]);

            if (!$existingFollow) {
                // Create a new follow relationship
                $follow = R::dispense('follow');
                $follow->follower = $followerAccount;
                $follow->followed = $followedAccount;

                // Store the follow relationship
                R::store($follow);

                http_response_code(200);
                echo json_encode(array(
                    'message' => "You are now following {$followedUsername}!",
                ));
            } else {
                http_response_code(400);
                echo json_encode(array(
                    'message' => "You are already following {$followedUsername}.",
                ));
            }
        } else {
            http_response_code(404);
            echo json_encode(array(
                'message' => "Follower or followed user not found.",
            ));
        }
    }

    public function unfollowUser($request): void
    {
        $followerAccountID = $request->user->account_id; // ID of the follower account
        $unfollowedUsername = $request->params[0]; // Username of the account to be unfollowed

        // Get the follower and unfollowed accounts
        $followerAccount = R::load('accounts', $followerAccountID);
        $unfollowedAccount = R::findOne('accounts', 'username = ?', [$unfollowedUsername]);

        if ($followerAccount && $unfollowedAccount) {
            // Check if the relationship exists
            $existingFollow = R::findOne('follow', 'follower_id = ? AND followed_id = ?', [$followerAccountID, $unfollowedAccount->id]);

            if ($existingFollow) {
                // Delete the follow relationship
                R::trash($existingFollow);

                http_response_code(200);
                echo json_encode(array(
                    'message' => "You have unfollowed {$unfollowedUsername}.",
                ));
            } else {
                http_response_code(400);
                echo json_encode(array(
                    'message' => "You were not following {$unfollowedUsername}.",
                ));
            }
        } else {
            http_response_code(404);
            echo json_encode(array(
                'message' => "Follower or unfollowed user not found.",
            ));
        }
    }

    public function getAccountStatuses($request): void
    {
        $statuses = R::find('status', 'WHERE account_id = ? ORDER BY id DESC LIMIT 100', [$request->params[0]]);

        // Set content-type to JSON
        header('Content-Type: application/json');

        $output = [];

        foreach ($statuses as $status) {
            $response = beanToMastoApiStatus($status, $request->user->account_id);
            // Add the status to the response array
            $output[] = $response;
        }

        echo json_encode($output, JSON_UNESCAPED_SLASHES );
    }

    public function getAccountbyId($request): void
    {
        $account = R::findOne('accounts', 'username = ? or id = ?',
            [$request->params[0], $request->params[0]]);

        if (!$account) {
            http_response_code(404);
            exit;
        }

        $response = beanToMastoApiAccount($account);

        http_response_code(200);
        header("Content-Type: application/json");

        echo json_encode($response, JSON_UNESCAPED_SLASHES);
    }

    public function updateAccountCredentials($request): void
    {
        $userId = $request->user->account_id;

        $userBean = R::load('users', $userId);
        $accountBean = R::load('accounts', $userBean->account_id);

        $booleanFields = [
            'accepts_chat_messages', 'allow_following_move', 'bot', 'discoverable',
            'hide_favorites', 'hide_followers', 'hide_followers_count', 'hide_follows',
            'hide_follows_count', 'locked', 'no_rich_text', 'show_birthday', 'show_role',
            'skip_thread_containment'
        ];

        foreach ($booleanFields as $field) {
            if (isset($request->body[$field])) {
                $accountBean->$field = filter_var($request->body[$field], FILTER_VALIDATE_BOOLEAN);
            }
        }

        if (isset($request->body['actor_type']) && in_array($request->body['actor_type'], ['Application', 'Group', 'Organization', 'Person', 'Service'])) {
            $accountBean->actor_type = $request->body['actor_type'];
        }

        if (isset($request->body['also_known_as']) && is_array($request->body['also_known_as'])) {
            $accountBean->also_known_as = json_encode($request->body['also_known_as']);
        }

        // For binary fields such as avatar, header, and pleroma_background_image, additional checks may be needed
        if (isset($request->body['avatar'])) {
            $accountBean->avatar = $request->body['avatar'];
        }

        if (isset($request->body['header'])) {
            $accountBean->header = $request->body['header'];
        }

        if (isset($request->body['birthday'])) {
            $accountBean->birthday = $request->body['birthday'];  // Ensure format validation here if needed
        }

        if (isset($request->body['default_scope']) && in_array($request->body['default_scope'], ['public', 'unlisted', 'local', 'private', 'direct', 'list'])) {
            $accountBean->default_scope = $request->body['default_scope'];
        }

        if (isset($request->body['display_name'])) {
            $accountBean->display_name = $request->body['display_name'];
        }

        // TODO: Validate fields_attributes if used in your application
        if (isset($request->body['fields_attributes']) && is_array($request->body['fields_attributes'])) {
            $accountBean->fields_attributes = json_encode($request->body['fields_attributes']);
        }

        if (isset($request->body['note'])) {
            $accountBean->note = $request->body['note'];
        }

        if (isset($request->body['pleroma_background_image'])) {
            $accountBean->pleroma_background_image = $request->body['pleroma_background_image'];
        }

        if (isset($request->body['pleroma_settings_store']) && is_array($request->body['pleroma_settings_store'])) {
            $accountBean->pleroma_settings_store = json_encode($request->body['pleroma_settings_store']);
        }

        try {
            R::begin();
            R::store($userBean);
            R::store($accountBean);
            R::commit();
        } catch (Exception $e) {
            R::rollback();
            echo array(
                'error' => array(
                    'message' => $e->getMessage()
                )
            );
        }

        $response = beanToMastoApiAccount($accountBean);
        header("HTTP/1.1 200 OK");
        header("Content-Type: application/json");
        echo json_encode($response, JSON_UNESCAPED_SLASHES);
    }

    public function registerAccount($request) {
        $username = $request->body['username'];
        $email = $request->body['email'];
        $password = $request->body['password'];

        // Check if username or email already exists
        if ($this->doesUserExist($username, $email)) {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode([
                "error" => "invalid_request",
                "error_description" => "Username or email already exists."
            ]);
            return;
        }

        // Hash the password
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

        // Insert into the database
        try {
            // Begin the transaction
            R::begin();

            // Insert into the users table
            $userBean = R::dispense('users');
            $userBean->username = $username;
            $userBean->email = $email;
            $userBean->password_hash = $hashedPassword;
            R::store($userBean);

            // Insert into the accounts table
            $accountBean = R::dispense('accounts');
            $accountBean->username = $username;
            $accountBean->instance = $_SERVER['HTTP_HOST'];
            $accountBean->display_name = $username;
            $accountBean->note = '';
            $accountBean->url = $GLOBALS["SCHEME"] . $_SERVER['HTTP_HOST'] . '/users/' . $username;
            R::store($accountBean);

            // Get the ID of the last inserted account
            $lastAccountId = $accountBean->id;

            // Update user.account_id with the last inserted account ID
            $userBean->account_id = $lastAccountId;
            R::store($userBean);

            // Commit the transaction
            R::commit();

            // Send success response to the user
            header("HTTP/1.1 201 Created");
            echo json_encode(beanToMastoApiAccount($accountBean));
        } catch (Exception $e) {
            // If any error occurs during the transaction, roll it back
            R::rollback();
            header("HTTP/1.1 500 Internal Server Error");
            echo json_encode([
                "error" => "server_error",
                "error_description" => "An error occurred while creating the account."
            ]);
        }
    }

    public function verifyCredentials($request) {
        $account = R::findOne('accounts', 'id = ?',
            [$request->user->account_id]);
        header("HTTP/1.1 200 Success");
        echo json_encode(beanToMastoApiAccount($account), JSON_UNESCAPED_SLASHES);
    }

    public function lookupAccount($request) {
        $username = $_GET['acct'] ?? null;  // Using $_GET to directly get the query parameter

        if (!$username) {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode([
                "error" => "invalid_request",
                "error_description" => "No account parameter provided."
            ]);
            return;
        }

        if ($this->doesUsernameExist($username)) {
            header("HTTP/1.1 200 OK");
            $account = R::findOne('accounts', 'username = ?', [$username]);
            echo json_encode(beanToMastoApiAccount($account));
        } else {
            header("HTTP/1.1 404 Not Found");
            echo json_encode([
                "error" => "not_found",
                "error_description" => "Account not found."
            ]);
        }
    }

    private function doesUsernameExist($username) {
        $count = R::count('users', 'username = ?', [$username]);
        return $count > 0;
    }

    private function doesUserExist($username, $email) {
        $count = R::count('users', 'username = ? OR email = ?', [$username, $email]);
        return $count > 0;
    }
}