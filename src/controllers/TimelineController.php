<?php

class TimelineController
{
    public function home($request) {
        return $this->public($request);
    }

    public function public($request) {
        // Fetch the last 100 statuses, ordering them by ID descending
        $statuses = R::find('status', 'ORDER BY id DESC LIMIT 100');

        // Set content-type to JSON
        header('Content-Type: application/json');

        $output = [];

        foreach ($statuses as $status) {
            // Get MastoAPI Status Object from bean
            $response = beanToMastoApiStatus($status, $request->user->account_id);
            // Add status to output array
            $output[] = $response;
        }

        echo json_encode($output, JSON_UNESCAPED_SLASHES );
    }
}

function isStatusFavorited($accountID, $statusID): bool
{
    // Check for an existing relationship between the account and the status in the favorite table
    $existingFavorite = R::findOne('favorite', 'account_id = ? AND status_id = ?', [$accountID, $statusID]);

    // Set content-type to JSON
    header('Content-Type: application/json');

    if ($existingFavorite) {
        return true;
    } else {
        return false;
    }
}

function isStatusBookmarked($accountID, $statusID): bool
{
    // Check for an existing relationship between the account and the status in the bookmark table
    $existingBookmark = R::findOne('bookmark', 'account_id = ? AND status_id = ?', [$accountID, $statusID]);

    // Set content-type to JSON
    header('Content-Type: application/json');

    if ($existingBookmark) {
        return true;
    } else {
        return false;
    }
}
