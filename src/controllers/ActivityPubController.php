<?php

class ActivityPubController
{
    function inbox($request): void
    {
        // Check if the request has a URL or URI
        $activityUrl = $request->body['id'] ?? null;

        if (!$activityUrl) {
            http_response_code(400);
            exit;
        }

        // Refetch the activity using curl
        $refetchedActivity = fetchActivity($activityUrl);

        if (!$refetchedActivity || !isValidActivityPubObject($refetchedActivity)) {
            http_response_code(400);
            exit;
        }

        switch ($refetchedActivity['type']) {
            case 'Person':
                handleAccountActivity($refetchedActivity);
                exit;
            case 'Note':
                handleStatusActivity($refetchedActivity);
                exit;
            case 'Create':
                handleCreateActivity($refetchedActivity);
                exit;
            case 'Update':
                handleUpdateActivity($refetchedActivity);
                exit;
            case 'Delete':
                handleDeleteActivity($refetchedActivity);
                exit;
            case 'Follow':
                handleFollowActivity($refetchedActivity);
                exit;
            case 'Accept':
                handleAcceptActivity($refetchedActivity);
                exit;
            case 'Reject':
                handleRejectActivity($refetchedActivity);
                exit;
            case 'Like':
                handleLikeActivity($refetchedActivity);
                exit;
            case 'Announce':
                handleAnnounceActivity($refetchedActivity);
                exit;
            case 'Undo':
                handleUndoActivity($refetchedActivity);
                exit;
            case 'Block':
                handleBlockActivity($refetchedActivity);
                exit;
            case 'Unblock':
                handleUnblockActivity($refetchedActivity);
                exit;
            case 'Report':
                handleReportActivity($refetchedActivity);
                exit;
            default:
                http_response_code(500);
                exit;
        }
    }

    public function getUser($request) {
        $username = $request->params[0];
        $user = R::findOne('users', 'username = ?', [$username]);

        // Check if the user exists in the database
        if (!$user) {
            http_response_code(404);
            echo json_encode(["error" => "User not found"]);
            return;
        }

        // Return the user
        $data = [
            "@context" => "https://www.w3.org/ns/activitystreams",
            "id" => "https://".$_SERVER["HTTP_HOST"]."/users/".$request->params[0],
            "type" => "Person",
            "following" => "https://".$_SERVER["HTTP_HOST"]."/users/".$request->params[0]."/following",
            "followers" => "https://".$_SERVER["HTTP_HOST"]."/users/".$request->params[0]."/followers",
            "inbox" => "https://".$_SERVER["HTTP_HOST"]."/users/".$request->params[0]."/inbox",
            "outbox" => "https://".$_SERVER["HTTP_HOST"]."/users/".$request->params[0]."/outbox",
            "preferredUsername" => $request->params[0],
            "endpoints" => [
                "sharedInbox" => "https://".$_SERVER["HTTP_HOST"]."/inbox"
            ],
            "url" => "https://".$_SERVER["HTTP_HOST"]."/@".$request->params[0]
        ];

        $json = json_encode($data, JSON_UNESCAPED_SLASHES);

        header('Content-Type: application/json');
        echo $json;
    }

    public function getOutbox($request) {
        // Extract actor and page from request (add validation as needed)
        $preferredUsername = $request->params[0];
        $page = isset($request->query['page']) ? intval($request->query['page']) : 1;

        // Fetch statuses (notes) for this actor
        // Here we're fetching the latest 10 statuses. Adjust as needed.
        $account = R::findOne('accounts', 'username = ?', [$preferredUsername]);

        if (!$account) {
            http_response_code(404);
            header('Content-Type: application/json');
            echo json_encode(array(
                'error' => array(
                    'message' => 'User not found!',
                )
            ));
            exit;
        }

        $accountId = $account->id;
        $itemsPerPage = 10; // Number of items per page
        $offset = ($page - 1) * $itemsPerPage;

        $items = R::find('status', 'account_id = ?', [$accountId], "LIMIT $itemsPerPage OFFSET $offset");

        $startsWith = $_SERVER['HTTP_HOST'];

        // Map each status to the desired format for ActivityPub
        $orderedItems = array_values(array_map(function($status) {
            return [
                '@context' => 'https://www.w3.org/ns/activitystreams',
                'id' => "http://localhost:8000/status/{$status->id}",
                'type' => 'Note',
                'content' => $status->status,
                'published' => $status->created_at,
            ];
        }, $items));

        // Return the ActivityPub outbox format
        $response = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "http://{$startsWith}/{$preferredUsername}/outbox",
            'type' => 'OrderedCollectionPage',
            'partOf' => "https://{$startsWith}/{$preferredUsername}/outbox",
            'next' => "https://{$startsWith}/users/{$preferredUsername}/outbox?page=" . ($page + 1),
            'prev' => $page > 1 ? "https://{$startsWith}/users/{$preferredUsername}/outbox?page=" . ($page - 1) : null,
            'orderedItems' => $orderedItems,
        ];
        header('Content-Type: application/json');
        echo json_encode($response, JSON_UNESCAPED_SLASHES);
    }

    public function getFollowers($request): void
    {
        $preferredUsername = $request->params[0]; // Username of the user whose followers you want to fetch

        $account = R::findOne('accounts', 'username = ?', [$preferredUsername]);

        if (!$account) {
            http_response_code(404);
            echo json_encode(array(
                'error' => array(
                    'message' => 'User not found!',
                )
            ));
            exit;
        }

        $accountID = $account->id;
        $followers = R::findAll('follow', 'followed_id = ?', [$accountID]);

        $startsWith = $_SERVER['HTTP_HOST'];

        // Map followers to the desired ActivityPub format
        $followersList = array_map(function ($follower) use ($startsWith) {
            $followerAccount = R::load('accounts', $follower->follower_id);

            return [
                '@context' => 'https://www.w3.org/ns/activitystreams',
                'id' => "http://{$startsWith}/users/{$followerAccount->username}",
                'type' => 'Person',
                'preferredUsername' => $followerAccount->username,
                'url' => $followerAccount->url,
            ];
        }, $followers);

        // Return the ActivityPub followers response
        $response = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "{$account->url}/followers",
            'type' => 'OrderedCollection',
            'totalItems' => count($followersList),
            'first' => "{$account->url}/followers",
            'last' => "{$account->url}/followers",
            'orderedItems' => $followersList,
        ];

        header('Content-Type: application/activity+json');
        echo json_encode($response, JSON_UNESCAPED_SLASHES);
    }

    public function getFollowing($request): void
    {
        $preferredUsername = $request->params[0]; // Username of the user whose following you want to fetch

        $account = R::findOne('accounts', 'username = ?', [$preferredUsername]);

        if (!$account) {
            http_response_code(404);
            echo json_encode(array(
                'error' => array(
                    'message' => 'User not found!',
                )
            ));
            exit;
        }

        $accountID = $account->id;
        $following = R::findAll('follow', 'follower_id = ?', [$accountID]);

        $startsWith = $_SERVER['HTTP_HOST'];

        // Map following to the desired ActivityPub format
        $followingList = array_map(function ($followed) use ($startsWith) {
            $followedAccount = R::load('accounts', $followed->followed_id);

            return [
                '@context' => 'https://www.w3.org/ns/activitystreams',
                'id' => "http://{$startsWith}/users/{$followedAccount->username}",
                'type' => 'Person',
                'preferredUsername' => $followedAccount->username,
                'url' => $followedAccount->url,
            ];
        }, $following);

        // Return the ActivityPub following response
        $response = [
            '@context' => 'https://www.w3.org/ns/activitystreams',
            'id' => "{$account->url}/following",
            'type' => 'OrderedCollection',
            'totalItems' => count($followingList),
            'first' => "{$account->url}/following",
            'last' => "{$account->url}/following",
            'orderedItems' => $followingList,
        ];

        header('Content-Type: application/activity+json');
        echo json_encode($response, JSON_UNESCAPED_SLASHES);
    }
}

function fetchActivity($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Accept: application/activity+json'
    ]);

    $response = curl_exec($ch);
    curl_close($ch);

    return json_decode($response, true);
}

function isValidActivityPubObject($object) : bool
{
    // List of known ActivityPub types
    $knownTypes = [
        "Note", "Person", "Create", "Like", "Follow",
        "Announce", "Accept", "Reject", "Undo"
    ];

    // Check for the presence of required properties
    if (!isset($object['type']) || !isset($object['id'])) {
        return false;
    }

    // Check if the type is one of the known ActivityPub types
    if (!in_array($object['type'], $knownTypes)) {
        return false;
    }

    // Check if the id is a valid URL
    if (!filter_var($object['id'], FILTER_VALIDATE_URL)) {
        return false;
    }

    // Ensure URLs use HTTPS for security
    if (parse_url($object['id'], PHP_URL_SCHEME) !== 'https') {
        return false;
    }

    // If it's a Note type, check for content and its length
    if ($object['type'] === "Note") {
        if (!isset($object['content']) || strlen($object['content']) > 5000) {  // Max length of 10000 chars
            return false;
        }
    }

    return true;
}

function handleAccountActivity($activity) {
    $existingAccount = R::findOne('accounts', 'url = ?', [$activity['id']]);
    if (!$existingAccount) {
        // Fetch the account
        $accountData = fetchActivity($activity['id']);
        if (!$accountData || !isValidActivityPubObject($accountData)) {
            return false;
        }

        $account = R::dispense('accounts');
        $account->username = $accountData['preferredUsername'];
        $account->instance = parse_url($accountData['id'], PHP_URL_HOST);
        $account->display_name = $accountData['name'];
        $account->note = $accountData['summary'];
        $account->url = $accountData['id'];
        $account->statuses_count = 0;

        R::store($account);
    }
    http_response_code(200);
    exit;
}

function handleStatusActivity($activity) {
    $existingAccount = R::findOne('accounts', 'url = ?', [$activity['attributedTo']]);
    if (!$existingAccount) {
        if (!handleAccountActivity(['id' => $activity['attributedTo'], 'type' => 'Person'])) {
            // Fail if the account doesn't exist and can't be created
            http_response_code(400);
            exit;
        }
        $existingAccount = R::findOne('accounts', 'url = ?', [$activity['attributedTo']]);
    }

    $status = R::dispense('status');
    $status->status = $activity['content'];
    $status->created_at = strtotime($activity['published']);
    $status->account_id = $existingAccount->id;
    $status->url = $activity['id'];
    $status->uri = $activity['id'];  // Assuming the URI is the same as the URL

    R::store($status);

    http_response_code(200);
    exit;
}

function handleCreateActivity($activity) {
    // Handle Create activity
    // Assuming the object of Create activity is a Note
    handleStatusActivity($activity['object']);
}

function handleUpdateActivity($activity) {
    // Handle Update activity
    // Assuming the object of Update activity is a Note
    $existingStatus = R::findOne('status', 'url = ?', [$activity['object']['id']]);
    if ($existingStatus) {
        $existingStatus->status = $activity['object']['content'];
        R::store($existingStatus);
    }
    http_response_code(200);
    exit;
}

function handleDeleteActivity($activity) {
    // Handle Delete activity
    // Assuming the object of Delete activity is a Note
    $existingStatus = R::findOne('status', 'url = ?', [$activity['object']['id']]);
    if ($existingStatus) {
        R::trash($existingStatus);
    }
    http_response_code(200);
    exit;
}

function handleFollowActivity($activity) {
    // Handle Follow activity
    $follower = R::findOne('accounts', 'url = ?', [$activity['actor']]);
    $followed = R::findOne('accounts', 'url = ?', [$activity['object']]);
    if ($follower && $followed) {
        $follow = R::dispense('follow');
        $follow->follower_id = $follower->id;
        $follow->followed_id = $followed->id;
        R::store($follow);
    }
    http_response_code(200);
    exit;
}

function handleAcceptActivity($activity) {
    // Handle Accept activity
    // Assuming the object of Accept activity is a Follow
    $follow = R::findOne('follow', 'id = ?', [$activity['object']['id']]);
    if ($follow) {
        $follow->accepted = true;
        R::store($follow);
    }
    http_response_code(200);
    exit;
}

function handleRejectActivity($activity) {
    // Handle Reject activity
    // Assuming the object of Reject activity is a Follow
    $follow = R::findOne('follow', 'id = ?', [$activity['object']['id']]);
    if ($follow) {
        R::trash($follow);
    }
    http_response_code(200);
    exit;
}

function handleLikeActivity($activity) {
    // Handle Like activity
    $existingAccount = R::findOne('accounts', 'url = ?', [$activity['actor']]);
    $likedStatus = R::findOne('status', 'url = ?', [$activity['object']]);
    if ($existingAccount && $likedStatus) {
        $like = R::dispense('like');
        $like->account_id = $existingAccount->id;
        $like->status_id = $likedStatus->id;
        R::store($like);
    }
    http_response_code(200);
    exit;
}

function handleAnnounceActivity($activity) {
    // Handle Announce activity
    // Assuming the object of Announce activity is a Note
    $existingAccount = R::findOne('accounts', 'url = ?', [$activity['actor']]);
    $announcedStatus = R::findOne('status', 'url = ?', [$activity['object']]);
    if ($existingAccount && $announcedStatus) {
        $announce = R::dispense('announce');
        $announce->account_id = $existingAccount->id;
        $announce->status_id = $announcedStatus->id;
        R::store($announce);
    }
    http_response_code(200);
    exit;
}

function handleUndoActivity($activity) {
    // Handle Undo activity
    // Assuming the object of Undo activity is a Like
    $like = R::findOne('like', 'id = ?', [$activity['object']['id']]);
    if ($like) {
        R::trash($like);
    }
    http_response_code(200);
    exit;
}

function handleBlockActivity($activity) {
    // Handle Block activity
    $blocker = R::findOne('accounts', 'url = ?', [$activity['actor']]);
    $blocked = R::findOne('accounts', 'url = ?', [$activity['object']]);
    if ($blocker && $blocked) {
        $block = R::dispense('block');
        $block->blocker_id = $blocker->id;
        $block->blocked_id = $blocked->id;
        R::store($block);
    }
    http_response_code(200);
    exit;
}

function handleUnblockActivity($activity) {
    // Handle Unblock activity
    // Assuming the object of Unblock activity is a Block
    $block = R::findOne('block', 'id = ?', [$activity['object']['id']]);
    if ($block) {
        R::trash($block);
    }
    http_response_code(200);
    exit;
}

function handleReportActivity($activity) {
    // Handle Report activity
    $reporter = R::findOne('accounts', 'url = ?', [$activity['actor']]);
    $reportedStatus = R::findOne('status', 'url = ?', [$activity['object']['id']]);
    if ($reporter && $reportedStatus) {
        $report = R::dispense('report');
        $report->reporter_id = $reporter->id;
        $report->status_id = $reportedStatus->id;
        $report->content = $activity['content'];
        R::store($report);
    }
    http_response_code(200);
    exit;
}
