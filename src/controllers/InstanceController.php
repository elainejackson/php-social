<?php

class InstanceController
{
    public function instance() {
        $protocol = "https";
        $domain = $_SERVER['HTTP_HOST'];

        $data = [
            "avatar_upload_limit" => 2000000,
            "background_image" => "",
            "background_upload_limit" => 4000000,
            "banner_upload_limit" => 4000000,
            "description" => "Instance powered by Celestia Social. An experimental fediverse server.",
            "email" => "defaultcontactaddress@celestia.social",
            "languages" => ["en"],
            "max_toot_chars" => 10000,
            "poll_limits" => [
                "max_expiration" => 31536000,
                "max_option_chars" => 200,
                "max_options" => 20,
                "min_expiration" => 0
            ],
            "registrations" => true,
            "stats" => [
                "domain_count" => 0,
                "status_count" => 0,
                "user_count" => 0
            ],
            "thumbnail" => "",
            "title" => "Celestia Social: An experimental fediverse server",
            "upload_limit" => 16000000,
            "uri" => "{$protocol}://{$domain}",
            "urls" => [
                "streaming_api" => "wss://{$domain}"
            ],
            "version" => "4.1.6 (compatible; Mastodon 4.1.6+soapbox)"
        ];

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function generateNodeInfoResponse() {
        $data = [
            'version' => '2.0',
            'software' => [
                'name' => 'PHP Social',
                'version' => '1.0.0',
            ],
            'protocols' => ['activitypub'],
            'services' => [
                'outbound' => [],
                'inbound' => [],
            ],
            'usage' => [
                'users' => [
                    'total' => 0,
                    'activeMonth' => 0,
                    'activeHalfyear' => 0,
                ],
                'localPosts' => 0,
            ],
            'openRegistrations' => true,
            'metadata' => [],
        ];

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function nodeinfo() {
        $host = $_SERVER['HTTP_HOST'];
        $data = [
            'links' => [
                [
                    'rel' => 'http://nodeinfo.diaspora.software/ns/schema/2.0',
                    'href' => "https://{$host}/nodeinfo/2.0",
                ],
            ],
        ];

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function hostmeta() {
        $currentHost = $_SERVER['HTTP_HOST'];
        $xmlContent = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
  <Link rel="lrdd" template="https://$currentHost/.well-known/webfinger?resource={uri}"/>
</XRD>
XML;

        header("Content-Type: application/xml");
        echo $xmlContent;
    }
}