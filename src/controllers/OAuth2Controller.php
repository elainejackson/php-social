<?php

class OAuth2Controller
{
    public function token($request) {
        $username = null;
        if (isset($request->body['username'])) {
            $username = $request->body['username'];
        }

        $password = null;
        if (isset($request->body['password'])) {
            $password = $request->body['password'];
        }

        $validCredentials = false;
        if (isset($username) && isset($password)) {
            $validCredentials = $this->checkCredentials($username, $password);
        }
        if ($validCredentials) {
            // Send the token
            header('Content-Type: application/json');
            $jwtClass = new JsonWebToken('changeme');
            echo json_encode([
                "access_token" => $jwtClass->create([
                    "identifier" => $username,
                ]),
                "token_type" => "Bearer",
                "scope" => "read write follow push",
                "created_at" => 0
            ]);
        } else if (!isset($username)) {
            http_response_code(200);
            $array = array(
                "error" => "invalid_grant",
                "error_description" => "The provided authorization grant is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client."
            );
            echo json_encode($array);
        } else {
            http_response_code(401);
            $array = array(
                "error" => "invalid_grant",
                "error_description" => "The provided authorization grant is invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another client."
            );
            echo json_encode($array);
        }
    }

    private function checkCredentials($identifier, $password): bool
    {
        // Using RedBeanPHP to fetch the user
        $userBean = R::findOne('users', 'username = ? OR email = ?', [$identifier, $identifier]);

        error_log($identifier);
        error_log($password);

        if ($userBean) {
            $storedHash = $userBean->password_hash;

            // Use password_verify() to check the password against the stored hash
            return password_verify($password, $storedHash);
        }

        return false; // User not found or password mismatch
    }
}