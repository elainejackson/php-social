<?php
require_once "lib/router.php";
require_once "lib/jsonwebtoken.php";
require_once "lib/BodyParser.php";
require_once "lib/DatabaseMiddleware.php";

require_once "transformers/beanToMastoApiStatus.php";
require_once "transformers/beanToMastoApiAccount.php";

require_once "controllers/ActivityPubController.php";
require_once "controllers/FakeAppController.php";
require_once "controllers/OAuth2Controller.php";
require_once "controllers/InstanceController.php";
require_once "controllers/WebFingerController.php";
require_once "controllers/AccountController.php";
require_once "controllers/TimelineController.php";
require_once "controllers/StatusController.php";
require_once "controllers/NotificationController.php";
require_once "controllers/ConversationController.php";

/**
 * Handle Prelight
 */
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('HTTP/1.1 200 OK');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header('Access-Control-Allow-Credentials: true');
    echo 'Success';
    exit();  // End the script execution for preflight request
}

$GLOBALS["SCHEME"] = 'https://'; 



/**
 * Setup Router
 */
$router = new Router();

/**
 * Middleware
 */
// Database
$router->middleware(new DatabaseMiddleware());

// BodyParser
$bodyParser = new BodyParser();
$router->middleware($bodyParser);

// JWT
$router->middleware(function($request) {
    // URLs to skip JWT verification
    $skipUrls = [
        '/^\/oauth\/token$/',
        '/^\/api\/pleroma\/captcha$/',
        '/^\/api\/v1\/accounts$/',
        '/^\/api\/v1\/accounts\/lookup\/?(\?.*)?$/',
        '/^\/api\/v1\/apps$/',
        '/^\/api\/v1\/instance$/',
        '/^\/api\/pleroma\/frontend_configurations$/',
    ];

    // Check the request URL against skipUrls patterns
    $currentUrl = $_SERVER['REQUEST_URI'];

    if (!str_starts_with($currentUrl, '/api')) {
        // Check if the URL is also not one of the exception URLs
        foreach ($skipUrls as $pattern) {
            if (preg_match($pattern, $currentUrl)) {
                return;  // It's an exception URL, skip JWT verification
            }
        }
        // It's not an API endpoint or an exception URL, so skip JWT verification
        return;
    }

    foreach ($skipUrls as $pattern) {
        if (preg_match($pattern, $currentUrl)) {
            return;  // Skip JWT verification and exit middleware
        }
    }

    // Check for Bearer token in the Authorization header
    $authHeader = $_SERVER['HTTP_AUTHORIZATION'] ?? '';
    $jwt = null;

    if (preg_match('/Bearer\s(\S+)/', $authHeader, $matches)) {
        $jwt = $matches[1];
    }

    // If no token was found, deny the request
    if (!$jwt) {
        header('HTTP/1.0 401 Unauthorized');
        echo "No token provided.";
        exit;
    }

    // Try to decode the token using the JsonWebToken class
    $jwtClass = new JsonWebToken('changeme');

    try {
        $decodedPayload = $jwtClass->validate($jwt);
        $identifier = $decodedPayload['identifier'];

        // Using RedBeanPHP to fetch the user
        $userBean = R::findOne('users', 'username = ? OR email = ?', [$identifier, $identifier]);

        if ($userBean) {
            // Convert the bean to a stdClass object
            $user = $userBean->export();

            // Attach the user object to the request
            $request->user = (object) $user;
        } else {
            // Handle the scenario where the user is not found, if needed
            // For instance:
            header('HTTP/1.0 401 Unauthorized');
            echo "User not found.";
            exit;
        }
    } catch (\Exception $e) {
        // Token validation error
        header('HTTP/1.0 401 Unauthorized');
        echo "Token error: " . $e->getMessage();
        exit;
    }
});

/**
 * Routes
 */

// App Controller
$appController = new FakeAppController();

$router->get('/api/v1/apps', [$appController, 'getApp']);
$router->post('/api/v1/apps', [$appController, 'postApp']);
$router->get('/api/v1/pleroma/apps', [$appController, 'getPleromaApps']);

// OAuth2 Controller
$oauthController = new OAuth2Controller();
$router->post('/oauth/token', [$oauthController, 'token']);
$router->get('/oauth/authorize', 'notImplementedResponse');
$router->post('/oauth/authorize', 'notImplementedResponse');

// WebFinger Controller
$webfingerController = new WebFingerController();
$router->get('/.well-known/webfinger', [$webfingerController, 'get']);

// Instance Controller
$instanceController = new InstanceController();
$router->get('/api/v1/instance', [$instanceController, 'instance']);
$router->get('/.well-known/nodeinfo', [$instanceController, 'nodeinfo']);
$router->get('/.well-known/host-meta', [$instanceController, 'hostmeta']);
$router->get('/nodeinfo/2.0', [$instanceController, 'generateNodeInfoResponse']);
$router->get('/nodeinfo/2.1.json',[$instanceController, 'generateNodeInfoResponse']);
$router->get('/api/v1/instance/peers','notImplementedResponse');
$router->get('/api/v1/pleroma/federation_status','notImplementedResponse');
$router->get('/manifest.json', 'notImplementedResponse');

// ActivityPub Controller
$activityPubController = new ActivityPubController();
$router->get('/users/:username', [$activityPubController, 'getUser']);
$router->get('/users/:username/outbox', [$activityPubController, 'getOutbox']);
$router->get('/users/:username/inbox', [$activityPubController, 'inbox']);
$router->get('/users/:username/followers', [$activityPubController, 'getFollowers']);
$router->get('/users/:username/following', [$activityPubController, 'getFollowing']);

// Inbox Controller
$router->post('/inbox', 'notImplementedResponse');

// Accounts Controller
$accountController = new AccountController();
$router->post('/api/v1/accounts', [$accountController, 'registerAccount']);
$router->post('/api/v1/follows', 'notImplementedResponse');
$router->get('/api/v1/accounts/verify_credentials', [$accountController, 'verifyCredentials']);
$router->patch('/api/v1/accounts/update_credentials', [$accountController, 'updateAccountCredentials']);
$router->get('/api/v1/accounts/relationships', 'notImplementedResponse');
$router->get('/api/v1/accounts/search','notImplementedResponse');
$router->get('/api/v1/accounts/lookup', [$accountController, 'lookupAccount']);

$router->post('/api/v1/accounts/:id/pin', 'notImplementedResponse');
$router->post('/api/v1/accounts/:id/mute', 'notImplementedResponse');
$router->post('/api/v1/accounts/:id/note', 'notImplementedResponse');
$router->post('/api/v1/accounts/:id/unpin', 'notImplementedResponse');
$router->post('/api/v1/accounts/:id/remove_from_followers', 'notImplementedResponse');
$router->post('/api/v1/accounts/:id/block', 'notImplementedResponse');
$router->post('/api/v1/accounts/:id/unblock', 'notImplementedResponse');
$router->post('/api/v1/accounts/:id/unmute', 'notImplementedResponse');
$router->post('/api/v1/accounts/:id/unfollow', [$accountController, 'unfollowUser']);
$router->post('/api/v1/accounts/:id/follow', [$accountController, 'followUser']);
$router->get('/api/v1/accounts/:id/identity_proofs','notImplementedResponse');
$router->get('/api/v1/accounts/:id/lists','notImplementedResponse');
$router->get('/api/v1/accounts/:id/following','notImplementedResponse');
$router->get('/api/v1/accounts/:id/statuses',[$accountController, 'getAccountStatuses']);

$router->get('/api/v1/accounts/:id', [$accountController, 'getAccountbyId']);

// Status Controller
$statusController = new StatusController();
$router->get('/api/v1/statuses/:id/context', [$statusController, 'getStatusContext']);
$router->post('/api/v1/statuses/:id/bookmark', [$statusController, 'bookmarkStatus']);
$router->post('/api/v1/statuses/:id/favourite', [$statusController, 'favoriteStatus']);
$router->post('/api/v1/statuses', [$statusController, 'createStatus']);
$router->delete('/api/v1/statuses/:id', [$statusController, 'deleteStatusById']);
$router->get('/api/v1/statuses/:id', [$statusController, 'getStatusById']);

// Scheduled Statuses
$router->get('/api/v1/scheduled_statuses/:id', 'notImplementedResponse');
$router->put('/api/v1/scheduled_statuses/:id', 'notImplementedResponse');
$router->delete('/api/v1/scheduled_statuses/:id', 'notImplementedResponse');
$router->get('/api/v1/scheduled_statuses', 'notImplementedResponse');

// Search
$router->get('/api/v1/search', 'notImplementedResponse');
$router->get('/api/v2/search', 'notImplementedResponse');

// Lists
$router->delete('/api/v1/lists/:id/accounts', 'notImplementedResponse');
$router->get('/api/v1/lists/:id/accounts', 'notImplementedResponse');
$router->post('/api/v1/lists/:id/accounts', 'notImplementedResponse');
$router->delete('/api/v1/lists/:id', 'notImplementedResponse');
$router->delete('/api/v1/lists/:id', 'notImplementedResponse');
$router->put('/api/v1/lists/:id', 'notImplementedResponse');
$router->get('/api/v1/lists', 'notImplementedResponse');
$router->post('/api/v1/lists', 'notImplementedResponse');

// Bookmarks and Favourites
$router->get('/api/v1/bookmarks', 'notImplementedResponse');
$router->get('/api/v1/favourites', 'notImplementedResponse');

// Polls Controller
$router->post('/api/v1/polls/:id/votes', 'notImplementedResponse');
$router->get('/api/v1/polls/:id', 'notImplementedResponse');

// Suggestions Controller
$router->delete('/api/v1/suggestions/:id', 'notImplementedResponse');
$router->get('/api/v2/suggestions', 'notImplementedResponse');

// Timelines Controller
$timelineController = new TimelineController();
$router->get('/api/v1/timelines/home', [$timelineController, 'home']);
$router->get('/api/v1/timelines/public', [$timelineController, 'public']);
$router->get('/api/v1/timelines/fediverse', 'notImplementedResponse');
$router->get('/api/v1/timelines/direct', 'notImplementedResponse');
$router->get('/api/v1/timelines/tag/:tag', 'notImplementedResponse');
$router->get('/api/v1/timelines/list/:list_id', 'notImplementedResponse');

// Conversations Controller
$conversationController = new ConversationController();
$router->delete('/api/v1/conversations/:id','notImplementedResponse');
$router->post('/api/v1/pleroma/conversations/read','notImplementedResponse');
$router->get('/api/v1/conversations',[$conversationController, 'getConversations']);
$router->post('/api/v1/conversations/:id/read','notImplementedResponse');
$router->get('/api/v1/pleroma/conversations/:id/statuses','notImplementedResponse');
$router->get('/api/v1/pleroma/conversations/:id','notImplementedResponse');
$router->patch('/api/v1/pleroma/conversations/:id','notImplementedResponse');

// Trends Controller
$router->get('/api/v1/trends/tags', 'notImplementedResponse');
$router->get('/v1/trends', 'notImplementedResponse');

// Blocks Controller
$router->get('/api/v1/mutes','notImplementedResponse');
$router->get('/api/v1/blocks','notImplementedResponse');

$router->delete('/api/v1/domain_blocks','notImplementedResponse');
$router->get('/api/v1/domain_blocks','notImplementedResponse');
$router->post('/api/v1/domain_blocks','notImplementedResponse');

// Filters Controller
$router->get('/api/v1/filters','notImplementedResponse');
$router->post('/api/v1/filters','notImplementedResponse');
$router->delete('/api/v1/filters/:id','notImplementedResponse');
$router->post('/api/v1/filters/:id','notImplementedResponse');
$router->put('/api/v1/filters/:id','notImplementedResponse');

// Follow Requests Controller
$router->post('/api/v1/follow_requests/:id/reject','notImplementedResponse');
$router->get('/api/v1/follow_requests','notImplementedResponse');
$router->post('/api/v1/follow_requests/:id/authorize','notImplementedResponse');

// Notifications Controller
$notificationController = new NotificationController();
$router->get('/api/v1/notifications/:id','notImplementedResponse');
$router->post('/api/v1/notifications/dismiss','notImplementedResponse');
$router->post('/api/v1/notifications/clear','notImplementedResponse');
$router->get('/api/v1/notifications', [$notificationController, 'getNotifications']);
$router->post('/api/v1/pleroma/notifications/read','notImplementedResponse');
$router->post('/api/v1/notifications/:id/dismiss','notImplementedResponse');
$router->delete('/api/v1/notifications/destroy_multiple','notImplementedResponse');

$router->delete('/api/v1/push/subscription','notImplementedResponse');
$router->get('/api/v1/push/subscription','notImplementedResponse');
$router->post('/api/v1/push/subscription','notImplementedResponse');
$router->put('/api/v1/push/subscription','notImplementedResponse');

// Reactions Controller
$router->delete('/api/v1/pleroma/statuses/:id/reactions/:emoji','notImplementedResponse');
$router->get('/api/v1/pleroma/statuses/:id/reactions/:emoji','notImplementedResponse');
$router->put('/api/v1/pleroma/statuses/:id/reactions/:emoji','notImplementedResponse');
$router->put('/api/v1/pleroma/statuses/:id/reactions','notImplementedResponse');

// Emoji Controller
$router->get('/api/v1/pleroma/emoji','notImplementedResponse');
$router->get('/api/v1/custom_emojis','notImplementedResponse');

// Pleroma Controller
$router->post('/api/pleroma/follow_import','notImplementedResponse');
$router->post('/api/pleroma/mutes_import','notImplementedResponse');
$router->post('/api/pleroma/blocks_import','notImplementedResponse');
$router->get('/api/v1/pleroma/birthdays','notImplementedResponse');
$router->post('/api/pleroma/disable_account','notImplementedResponse');
$router->post('/api/pleroma/delete_account','notImplementedResponse');
$router->post('/api/pleroma/change_password','notImplementedResponse');
$router->post('/api/v1/pleroma/accounts/confirmation_resend','notImplementedResponse');
$router->post('/api/pleroma/change_email', 'notImplementedResponse');
$router->post('/api/pleroma/move_account','notImplementedResponse');
$router->delete('/api/pleroma/aliases','notImplementedResponse');
$router->get('/api/pleroma/aliases','notImplementedResponse');
$router->put('/api/pleroma/aliases','notImplementedResponse');
$router->post('/api/v1/pleroma/backups','notImplementedResponse');
$router->get('/api/v1/pleroma/accounts/:id/favourites','notImplementedResponse');
$router->post('/api/v1/pleroma/accounts/:id/unsubscribe', 'notImplementedResponse');
$router->post('/api/v1/pleroma/accounts/:id/subscribe', 'notImplementedResponse');

// Announcements Controller
$router->get('/api/v1/announcements', 'notImplementedResponse');
$router->post('/api/v1/announcements/:id/dismiss', 'notImplementedResponse');

////
//    IMPLEMENT THESE
//
//    These are mostly Pleroma specific API Endpoints (with a few exceptions)
//    I'll implement these after everything else gets implemented to hopefully make Soapbox better.
////

$router->get('/api/v1/accounts/:id/followers','notImplementedResponse');
$router->get('/api/v1/pleroma/accounts/:id/endorsements','notImplementedResponse');
$router->get('/api/v1/endorsements','notImplementedResponse');


$router->get('/api/v1/pleroma/healthcheck','notImplementedResponse');
$router->get('/api/v1/directory','notImplementedResponse');
$router->get('/api/v1/pleroma/captcha','notImplementedResponse');
$router->get('/api/pleroma/frontend_configurations','notImplementedResponse');

$router->post('/api/v1/pleroma/remote_interaction','notImplementedResponse');
$router->get('/main/ostatus','notImplementedResponse');
$router->post('/main/ostatus','notImplementedResponse');

$router->get('/api/v0/pleroma/reports/:id', 'notImplementedResponse');
$router->get('/api/v0/pleroma/reports', 'notImplementedResponse');
$router->post('/api/v1/reports', 'notImplementedResponse');

$router->get('/api/v1/pleroma/settings/:app', 'notImplementedResponse');
$router->patch('/api/v1/pleroma/settings/:app', 'notImplementedResponse');

// Dispatch the request to the appropriate route.
$router->dispatch();

function notImplementedResponse(): void
{
    header('HTTP/1.1 404 Not Found');
    echo "Not implemented!";
}